//
//  Book.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

struct Book : Decodable {
    let title : String!
    let author_name : [String]!
    let authorKey : [String]!
    let lastModified : Float? = nil
    let cover_edition_key : String?
    let cover_i : Int?
}

struct JSONResponse : Decodable{
    let docs : [Book]!
}

//image reference for loading
//https://covers.openlibrary.org/b/olid/OL1532643M-M.jpg
//https://covers.openlibrary.org/w/id/8314541-L.jpg

// Data field reference for parsing
//{
//doc:[
//{
//    "title_suggest": "The Lord of the Rings",
//    "has_fulltext": false,
//    "edition_count": 0,
//    "last_modified_i": 1546454460,
//    "title": "The Lord of the Rings",
//    "author_alternative_name": [
//    "J. R. R. Tolkein",
//    "John Ronald Reuel Tolkien",
//    "J.R.R. Tolkein"
//    ],
//    "seed": [
//    "/works/OL14926034W",
//    "/authors/OL26320A"
//    ],
//    "author_name": [
//    "J. R. R. Tolkien"
//    ],
//    "key": "/works/OL14926034W",
//    "text": [
//    "J. R. R. Tolkien",
//    "OL26320A",
//    "The Lord of the Rings",
//    "/works/OL14926034W",
//    "J. R. R. Tolkein",
//    "John Ronald Reuel Tolkien",
//    "J.R.R. Tolkein"
//    ],
//    "author_key": [
//    "OL26320A"
//    ],
//    "type": "work",
//    "ebook_count_i": 0
//}
//]
