//
//  BookCell.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {

    var titleLabel = UILabel()
    var authorLabel = UILabel()
    var coverImageView = UIImageView()
    
    var data : Book? {
        didSet {
            guard let temp = data else{
                return
            }
            titleLabel.text = temp.title
            if temp.author_name != nil {
                authorLabel.text = temp.author_name.joined(separator: ", ")
            }
            
            var imageUrl = ""
            if let cover = temp.cover_i{
                imageUrl = getUrlForSmallImageWithKey(key: String(cover))
                coverImageView.image = ImageDownloader.Instance.getCacheImage(urlString: imageUrl)
            }
            if imageUrl == "" && temp.cover_edition_key != nil{
                imageUrl = getUrlForLargeImageWithKey(key: String(temp.cover_edition_key!))
                coverImageView.image = ImageDownloader.Instance.getCacheImage(urlString: imageUrl)
            }
            
        }
    }
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutMargins = UIEdgeInsets.zero
        setupLayout()
        applyConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    //MARK: Private
    func setupLayout() {
        
        selectionStyle = .none
        //nameLabel
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        addSubview(titleLabel)
        
        authorLabel.numberOfLines = 0
        addSubview(authorLabel)
        
        //image
//        coverImageView.backgroundColor = .blue
        coverImageView.contentMode = .scaleAspectFit
        addSubview(coverImageView)
    }
    
    func applyConstraints(){
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.leadingAnchor.constraint(equalTo: (coverImageView.superview?.leadingAnchor)!, constant: 8).isActive = true
        coverImageView.centerYAnchor.constraint(equalTo: (coverImageView.superview?.centerYAnchor)!).isActive = true
        coverImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        let bottom = coverImageView.bottomAnchor.constraint(equalTo:(titleLabel.superview?.bottomAnchor)!)
        bottom.priority = .required
        bottom.isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: coverImageView.trailingAnchor, constant:5).isActive = true
        titleLabel.topAnchor.constraint(equalTo: (titleLabel.superview?.topAnchor)!, constant:5).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: (titleLabel.superview?.trailingAnchor)!).isActive = true
        
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.leadingAnchor.constraint(equalTo: coverImageView.trailingAnchor, constant:5).isActive = true
        authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant:0).isActive = true
        authorLabel.trailingAnchor.constraint(equalTo: (authorLabel.superview?.trailingAnchor)!).isActive = true
        authorLabel.bottomAnchor.constraint(equalTo: (authorLabel.superview?.bottomAnchor)!, constant: -15).isActive = true
        
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
