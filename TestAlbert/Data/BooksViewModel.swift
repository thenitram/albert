//
//  BooksViewModel.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit
import CoreData

class BooksViewModel: NSObject {

    //MARK: - Properties
    private var refreshCallback : (()->Void)?
    var books = [Book]()
    var filtered : [Book] = [] {
        didSet {
            if oldValue.count == 0{
                filtered = books
            }
        }
    }
    
    //MARK: -
    required init(_ fetchCompletionHandlder : (()->Void)? = nil) {
        refreshCallback = fetchCompletionHandlder
    }
    
    func runCallback(){
        DispatchQueue.main.async {
            self.refreshCallback?()
        }
    }

    //MARK: - Web request to get json data
    func requestData(query : String!){
            let callback : ((JSONResponse?)->Void)? = {[unowned self] (data) in
            if let books = data?.docs{
                self.books = books
                self.filtered = books
                self.runCallback()
            }
        }
        let stringQuery = "https://openlibrary.org/search.json?q=\(query ?? "")"
        fetchJsonData(urlString: stringQuery, completion: callback)
    }
    
    //MARK: - CoreData stuff
    
    //Saves data locally
    func saveToWishList(book : Book){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Libro", in: context)
        let libro = NSManagedObject(entity: entity!, insertInto: context)
        
        //Store data
        if let authors = book.author_name{
            let encodedAuthors = Data(base64Encoded: authors.joined(separator: "-"))
            libro.setValue(encodedAuthors, forKey: "author")
        }
        libro.setValue(book.title, forKey: "title")
        libro.setValue(book.cover_i ?? 0, forKey: "cover_i")
        libro.setValue(book.cover_edition_key ?? "", forKey: "cover_edition_key")

        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func fetchSavedData() {
                
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Libro")
        request.returnsObjectsAsFaults = false
        
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                //parse properties
                let title = data.value(forKey: "title") as? String
                var arrayAuthors : [String]?
                if let encodedData = data.value(forKey: "author") as? Data{
                    let decodedAuthors = String(data: encodedData, encoding: .utf8)!
                    arrayAuthors = decodedAuthors.split(separator: "-").map(String.init)
                }
                let cover_i = data.value(forKey: "cover_i") as? Int
                let cover_edition_key = data.value(forKey: "cover_edition_key") as? String
                
                self.books.append(Book(title: title, author_name: arrayAuthors, authorKey: nil, cover_edition_key: cover_edition_key, cover_i: cover_i))
            }
            
            self.filtered = books
            self.runCallback()
        } catch {
            print("Failed")
        }
    }
}
