//
//  BookViewModel.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

class BookViewModel: NSObject {

    var bookTitle : String!
    var bookAuthor : String!
    var cover : String?
    var book : Book! {
        didSet{
            self.bookTitle = book.title
            self.bookAuthor = (book.author_name != nil) ? book.author_name.joined(separator: " ") :""
            if let coverKey = book.cover_i{
                self.cover = getUrlForSmallImageWithKey(key: "\(coverKey)")
            }
            
            if self.cover == nil && book.cover_edition_key != nil{
                self.cover = getUrlForLargeImageWithKey(key: "\(book.cover_edition_key!)")
            }
        }
    }
}
