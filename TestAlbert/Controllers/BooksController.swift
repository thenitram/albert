//
//  BooksController.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit
import CoreData

class BooksController: UIViewController {

    //MARK: - Properties
    var booksViewModel : BooksViewModel!
    let cellID = "bookCell"
    var searchController = UISearchController()
    var tableView = UITableView()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        setup()
    }
    
    //MARK: - Setup
    func setup(){
        prepareData()
        prepareSearchController()
        prepareTableView()
        applyConstraints()
    }
    
    func prepareSearchController(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.frame = CGRect(x: 0, y: 0, width: 320, height: 50)

        searchController.searchBar.delegate = self
        searchController.searchBar.returnKeyType = UIReturnKeyType.done
        searchController.searchBar.enablesReturnKeyAutomatically = false
    }
    
    func prepareTableView(){
        tableView.register(BookCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.tableHeaderView = searchController.searchBar
    }
    
    //Constraints
    func applyConstraints(){
        //TableView Constraints
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: (tableView.superview?.topAnchor)!, constant:5).isActive = true
        tableView.leadingAnchor.constraint(equalTo: (tableView.superview?.leadingAnchor)!).isActive = true
        tableView.trailingAnchor.constraint(equalTo: (tableView.superview?.trailingAnchor)!).isActive = true
        tableView.bottomAnchor.constraint(equalTo: (tableView.superview?.bottomAnchor)!, constant: -5).isActive = true
    }
    
    func prepareData(){
        booksViewModel = BooksViewModel{
            DispatchQueue.main.async(execute: { [weak self] in
                self?.reloadData()
            })
        }
        booksViewModel.requestData(query:"latest")
    }
    
    func reloadData(){
        tableView.reloadData()
        
        //Move this block away
        for i in 0..<booksViewModel.filtered.count{
            let data = booksViewModel.filtered[i]
            guard let stringUrl = data.cover_i else {return}
            let imageUrl = getUrlForSmallImageWithKey(key: "\(stringUrl)")
            ImageDownloader.Instance.loadImageWithUrl(urlString: imageUrl, completetion: {(image) in
                DispatchQueue.main.async{[weak self] in
                    self?.tableView.reloadRows(at: [IndexPath(row: i, section:0)], with: .automatic)
                }
            })
        }
    }
}


