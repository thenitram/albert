//
//  MainTabbarController.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

class MainTabbarController: UITabBarController, UITabBarControllerDelegate {

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
    }
    
    //MARK: - Setup
    func setupTabBar() {
        view.tintColor = .black
        view.backgroundColor = .clear
        delegate = self
        
        //Instantiate controllers
        let booksController = createController(viewController: BooksController(), selectedImage: UIImage(named: "tabBarWhatsHot_p") ?? UIImage(), unselectedImage: UIImage(named: "tabBarWhatsHot") ?? UIImage())
        let wishListController = createController(viewController: WishListController(), selectedImage: UIImage(named: "tabBarWeLove_p") ?? UIImage(), unselectedImage: UIImage(named: "tabBarWeLove") ?? UIImage())

        //add controllers to tabbar
        viewControllers = [booksController, wishListController]
    }
    
    //MARK: - Delegates
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false
        }
        
        if(fromView == toView) {
            return false
        }
        
        //Animate transistion
        UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        return true
    }
}


//Tabber helper
extension MainTabbarController{
    
    func createController(viewController: UIViewController, selectedImage: UIImage, unselectedImage: UIImage) -> UIViewController{
        let controller = UINavigationController(rootViewController: viewController)
        controller.tabBarItem.image = unselectedImage.withRenderingMode(.alwaysOriginal)
        controller.tabBarItem.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
        controller.tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        return controller
    }
}
