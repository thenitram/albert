//
//  WishListController.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

class WishListController: BooksController {
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareData()
    }
    
    override func setup() {
        super.setup()
        view.backgroundColor = .blue
        tableView.tableHeaderView = nil //Removes the searchbar since we dont need it in wishlist controller.
    }
    
    override func prepareData() {
        booksViewModel = BooksViewModel{
            DispatchQueue.main.async(execute: { [weak self] in
                self?.reloadData()
            })
        }
        booksViewModel.fetchSavedData()
    }
    
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //TODO: Find a unique key for deletion.
            // Delete the row from the data source
            booksViewModel.filtered.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
