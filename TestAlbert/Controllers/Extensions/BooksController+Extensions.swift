//
//  BooksController+Extensions.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit


//MARK: - SearchController
extension BooksController : UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All"){
//        booksViewModel.filtered = booksViewModel.books.filter{
//            book in return
//            book.title.lowercased().contains(searchText.lowercased())
//        }
//        reloadData()
    }
    
}

extension BooksController : UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.text = nil
        searchController.searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        booksViewModel.requestData(query: searchText)
    }
    
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar){
//        if searchBar.text?.count == 0 {
//            booksViewModel.filtered = booksViewModel.books
//            reloadData()
//        }
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        booksViewModel.filtered = booksViewModel.books
//        tableView.reloadData()
//    }
}


//MARK: - Tableview
extension BooksController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksViewModel.filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as? BookCell{
            let book = booksViewModel.filtered[indexPath.row]
            cell.data = book
            if indexPath.row % 2 == 0 {
                cell.contentView.backgroundColor = UIColor.init(red: 8, green: 2, blue: 0.2, alpha: 0.5)
            }
            else{
                cell.contentView.backgroundColor = UIColor.init(red: 2, green: 2, blue: 0.8, alpha: 0.8)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = DetailedViewController() //TODO: Make this a protocol
        let bookViewModel = BookViewModel()
        bookViewModel.book = booksViewModel.filtered[indexPath.row] //TODO: Fix so that this controller dont need to access book
        controller.bookViewModel = bookViewModel
        present(controller, animated: true, completion: nil)

        booksViewModel.saveToWishList(book: bookViewModel.book)
    }
}
