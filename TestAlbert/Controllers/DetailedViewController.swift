//
//  DetailedViewController.swift
//  TestAlbert
//
//  Created by Martin on 4/24/19.
//  Copyright © 2019 Martin. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController {

    var bookViewModel : BookViewModel!
    
    //MARK: - Properties
    var coverImageView = UIImageView()
    var bookTitleLabel = UILabel()
    var bookAuthorLabel = UILabel()
    var backButton = UIButton(type: .custom)
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    //MARK: - Setup
    func setupLayout() {
        
        view.backgroundColor = .lightGray

        //close buton
        backButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        backButton.setTitle("X", for: .normal)
        bookTitleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        view.addSubview(backButton)

        //book titleLabel
        bookTitleLabel.text = bookViewModel.bookTitle
        bookTitleLabel.numberOfLines = 0
        bookTitleLabel.textColor = .white
        bookTitleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        bookTitleLabel.textAlignment = .center
        view.addSubview(bookTitleLabel)
        
        //book authorLabel
        bookAuthorLabel.text = bookViewModel.bookAuthor
        bookAuthorLabel.numberOfLines = 0
        bookAuthorLabel.font = UIFont.boldSystemFont(ofSize: 13)
        bookAuthorLabel.textAlignment = .center
        view.addSubview(bookAuthorLabel)
        
        //imageView
        coverImageView.image = UIImage(named: "test1")
        coverImageView.backgroundColor = .blue
        coverImageView.contentMode = .scaleAspectFit
        view.addSubview(coverImageView)
        
        //Load book image
//        let imageUrl = getUrlForSmallImageWithKey(key: bookViewModel.cover ?? "")
        ImageDownloader.Instance.loadImageWithUrl(urlString:bookViewModel.cover ?? "") { (image) in
            DispatchQueue.main.async { [weak self] in
                self?.coverImageView.image = image
            }
        }
        
        applyConstraints()
    }
    
    //Constraints
    func applyConstraints(){
        //Close button constraints
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.topAnchor.constraint(equalTo: (backButton.superview?.topAnchor)!, constant:25).isActive = true
        backButton.trailingAnchor.constraint(equalTo: (backButton.superview?.trailingAnchor)!, constant:-25).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 28).isActive = true
        backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor).isActive = true
        
        //Cover image constraints
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.topAnchor.constraint(equalTo: (coverImageView.superview?.topAnchor)!, constant: 44).isActive = true
        coverImageView.centerXAnchor.constraint(equalTo: (coverImageView.superview?.centerXAnchor)!).isActive = true
        coverImageView.widthAnchor.constraint(equalToConstant: 180).isActive = true
        coverImageView.heightAnchor.constraint(equalToConstant: 260).isActive = true
        
        //title constraints
        bookTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        bookTitleLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor, constant:15).isActive = true
        bookTitleLabel.leadingAnchor.constraint(equalTo: (bookTitleLabel.superview?.leadingAnchor)!).isActive = true
        bookTitleLabel.trailingAnchor.constraint(equalTo: (bookTitleLabel.superview?.trailingAnchor)!).isActive = true
        bookTitleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //author label constraints
        bookAuthorLabel.translatesAutoresizingMaskIntoConstraints = false
        bookAuthorLabel.topAnchor.constraint(equalTo: bookTitleLabel.bottomAnchor, constant:5).isActive = true
        bookAuthorLabel.leadingAnchor.constraint(equalTo: (bookAuthorLabel.superview?.leadingAnchor)!).isActive = true
        bookAuthorLabel.trailingAnchor.constraint(equalTo: (bookAuthorLabel.superview?.trailingAnchor)!).isActive = true
        bookAuthorLabel.bottomAnchor.constraint(equalTo: (bookAuthorLabel.superview?.bottomAnchor)!).isActive = true
    }
    
    //MARK: - Custom methods
    @objc func close(){
        dismiss(animated: true, completion: nil)
    }
}
