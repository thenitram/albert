//
//  ImageDownloader.swift
//  basic
//
//  Created by Martin on 2/20/19.
//

import UIKit

func fetchJsonData<T:Decodable>(urlString :String!, completion: ((T?)->Void)? = nil){
    
    //Get instance of URLSession
    let session = URLSession.shared
    
    //Prepare URLRequest
    guard let url = URL(string: urlString) else{
        return
    }
    let request = URLRequest(url: url)
    
    //Initiate request using SessionDataTask
    let task = session.dataTask(with: request) { (data, response, error) in
        
        if error != nil{
            print("Error : \(error!.localizedDescription)")
            return
        }
        
        do{
            guard let data = data else { return }
            let parsedData = try JSONDecoder().decode(T.self, from: data)
            completion?(parsedData)
        }
        catch { }
    }
    task.resume()
}
