//
//  ImageDownloader.swift
//  basic
//
//  Created by Martin on 6/20/18.
//

import UIKit
let imageCache = NSCache<NSString, UIImage>()
typealias DownloadCallback = (UIImage?)->()
typealias ImageDownloadResult = (url:String, image:UIImage?, error:Error?)

fileprivate protocol ImageDownloaderDelegate {
    func imageDownloaded(result : ImageDownloadResult)
}

class ImageDownloadTask {
    var url : URL
    var session : URLSession
    
    fileprivate var task: URLSessionDownloadTask?
    fileprivate var resumeData: Data?
    fileprivate var isDownloading = false
    fileprivate var isFinishedDownloading = false
    fileprivate var delegate: ImageDownloaderDelegate?
    fileprivate var dateStarted : TimeInterval?
    
    init(urlString : String) {
        url = URL(string: urlString)!
        session = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    func resume() {
        let now = Date().timeIntervalSince1970
        if(dateStarted != nil && (now - dateStarted!) > 30){
            let error = NSError(domain:"Force timeout", code:504, userInfo:nil)
            delegate?.imageDownloaded(result: (url.absoluteString, nil, error))
            reset()
            return
        }
        
        //Start or Continue download
        if !isDownloading && !isFinishedDownloading {
            isDownloading = true
            
            if let resumeData = resumeData {
                task = session.downloadTask(withResumeData: resumeData, completionHandler: downloadTaskCompletionHandler)
            } else {
                dateStarted = Date().timeIntervalSince1970
                task = session.downloadTask(with: url, completionHandler: downloadTaskCompletionHandler)
            }
            task?.resume()
        }
    }
    
    func pause() {
        if isDownloading && !isFinishedDownloading {
            task?.cancel(byProducingResumeData: { (data) in
                self.resumeData = data
            })
            
            self.isDownloading = false
        }
    }
    
    func reset() {
        isDownloading = false
        isFinishedDownloading = false
        delegate = nil
        dateStarted = nil
        resumeData = nil
    }
    
    private func downloadTaskCompletionHandler(url: URL?, response: URLResponse?, error: Error?) {
        if let error = error {
            print("Error downloading: ", error)
            reset()
            return
        }

        guard let url = url else {
            reset()
            return
        }
        guard let data = FileManager.default.contents(atPath: url.path) else {
            reset()
            return
        }
        guard let image = UIImage(data: data) else {
            let error = NSError(domain: "Can't extract image.", code: 1000, userInfo: nil)
            delegate?.imageDownloaded(result: ((response?.url?.absoluteString)!, nil, error))
            reset()
            return
        }
        
        imageCache.setObject(image, forKey: (response?.url?.absoluteString)! as NSString)
        delegate?.imageDownloaded(result: ((response?.url?.absoluteString)!, image, error))
        reset()
    }
}

class ImageDownloader : ImageDownloaderDelegate{
    
    static let Instance = ImageDownloader()
    fileprivate var tasks = [String : ImageDownloadTask]()
    fileprivate var listCallback = [String : DownloadCallback?]()
    
    fileprivate init() {
        
    }
    
    func loadImageWithUrl(urlString: String, completetion: DownloadCallback?){
        if urlString.count == 0 { return }
        listCallback[urlString] = completetion

        //Check cache for image first
        if let image = getCacheImage(urlString: urlString){
            if let callback = listCallback[urlString]{
                callback?(image)
            }
            return
        }

        if tasks[urlString] == nil{
            let task = ImageDownloadTask(urlString: urlString)
            tasks[urlString] = task
            task.delegate = self
        }
        //resume is new task or already existing
        resume(urlString: urlString)
    }
    
    func getCacheImage(urlString : String) -> UIImage?{
        return imageCache.object(forKey: urlString as NSString)
    }
    
    func resume(urlString: String){
        tasks[urlString]?.resume()
    }
    
    func pause(urlString: String){
        tasks[urlString]?.pause()
    }
    
    //delegate
    fileprivate func imageDownloaded(result: ImageDownloadResult) {
        if let callback = listCallback[result.url] {
            callback?(result.image)
        }
    }
}

//TODO: Remove this here
func getUrlForSmallImageWithKey(key :String) -> String{
    return "https://covers.openlibrary.org/w/id/\(key)-M.jpg"
}
//TODO: Remove this here
func getUrlForLargeImageWithKey(key :String) -> String{
    return "https://covers.openlibrary.org/w/id/\(key)-L.jpg"
}
